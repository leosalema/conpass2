## Objetivo

Esse projeto foi desenvolvido para gerenciar Hotspots. <br>
<br>
Ao clicar para registrar um novo hotspot a aplicação fará um registro do próximo clique do mouse e irá inserir uma marcação no local do clique e um registro na tabela abaixo.<br>

Após o registro será possível o usuário clicar na marcação e adicionar um comentário.<br>
<br>
Ao passar com o mouse sobre o item da tabela será exibido uma mensagem.<br>

## Instalação
Tenha instalado em sua máquina Node js.<br>
Execute `npm install` para instalar as dependências.<br>
Exevute `npm start` para testar a aplicação.<br>

Abra [http://localhost:3000](http://localhost:3000) para visualizar em seu browser.<br>

## Pacotes
 - create-react-app
 - @material-ui/core
 - @material-ui/icons
 - bootstrap
 - redux
 - react-redux
 - reactstrap
 - redux
 - redux-form
 - redux-promise
