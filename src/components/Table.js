/**
 * A tabela recebe da store os dados registrados
 * 
 * a lista é exibida na tela e cada um item dela
 * possui um botão para exclusão, além disso eles possuem
 * um evento de leitura do movimento do mouse para demonstrativo 
 * de uma mensagem.
 * 
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {deleteHotspot} from '../actions/mainActions'
import Delete from '@material-ui/icons/Delete'

class Table extends Component {
  handleMouse = () => 
    alert(this.props.item.content)

  render () {
    return (
      <div className="table-responsive-lg ">
        <table className="table">
          <tbody>
            {this.props.list.map (item => (
              <tr key={item._id}>
                <td onMouseOver={this.handleMouse}>{item.title}</td>
                <td><Delete onClick={() => this.props.deleteHotspot(item)}/></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({list: state.main.list});
const mapDispatchToProps = dispatch => bindActionCreators({deleteHotspot}, dispatch)

export default connect (mapStateToProps, mapDispatchToProps) (Table);
