/**
 * O Header foi feito estático apenas para demonstração.
 */

import React from 'react';
import conpass from '../images/conpass.png'

export default props => (
  <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <img src={conpass} alt='Logo Conpass' />
    <button
      className="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#navbarNav"
      aria-controls="navbarNav"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span className="navbar-toggler-icon" />
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav">
        <li className="nav-item active">
          <span className="nav-link">LinkFake 1</span>
        </li>
        <li className="nav-item active">
          <span className="nav-link">LinkFake 2</span>
        </li>
        <li className="nav-item active">
          <span className="nav-link">LinkFake 3</span>
        </li>
        <li className="nav-item active">
          <span className="nav-link">LinkFake 4</span>
        </li>
        <li className="nav-item active">
          <span className="nav-link">LinkFake 5</span>
        </li>
      </ul>
    </div>
  </nav>
);
