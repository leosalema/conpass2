/**
 * O componente ButtonPulse foi criado para 
 * apresentar um circulo vermelho com um anel a sua volta e ao ser 
 * clicado envia o id de qual popover deverá ser aberto.
 */

import React, {Component} from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {openPover } from '../actions/mainActions';
import '../styles/buttonPulse.css';
import Popovers from './Popovers'

class ButtonPulse extends Component {
  render () {
    return (
      <div>
        <section
          className="btn-pulse"
          id={`Popover${this.props.item._id}`}
          style={{top: this.props.item.top, left: this.props.item.left}}
          onClick={() => this.props.openPover(this.props.item._id)}
        >
          <span className="ring" />
          <span className="circle" />
        </section>
        <Popovers item={this.props.item}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({ statePover: state.main.statePover, list: state.main.list })
const mapDispatchToProps = dispatch => bindActionCreators({openPover}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ButtonPulse)
