/**
 * O popover foi criado para permitir o cadastro e depois a 
 * apresentação do conteúdo explicativo para a o item marcado.
 * 
 * É feito uma verificação inicial para saber se o popover a ser aberto
 * é mesmo aquele que foi clicado, isso foi feito para remover um bug que fazia com 
 * que todos os popovers fossem apresentados.
 * 
 * Abaixo é feita outra verificação para saber se ele já possui conteúdo cadastrado,
 * casa não tenha é apresentado o formulário para cadastro, caso contrário é apresentado
 * o conteúdo cadastro.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Field, reduxForm } from 'redux-form'
import {Popover, PopoverHeader, PopoverBody, Form, FormGroup, Input, Button} from 'reactstrap';
import {openPover, changeContent, handleSubmit} from '../actions/mainActions';

class Popovers extends Component {
  keyHandler = (event) => {
    if(event.key === 'Enter')
      this.props.handleSubmit(this.props.item, this.props.content)
    else if (event.key === 'Escape')
      return this.props.reset()
  }

  render () { 
    if (this.props.idPover === this.props.item._id) {
      return (
        this.props.item.content === '' ?
        <Popover
          placement="bottom"
          isOpen={this.props.statePover}
          target={`Popover${this.props.item._id}`}
        >
          <PopoverHeader>{this.props.item.title}</PopoverHeader>
          <PopoverBody>
            <Form onSubmit={() => this.props.handleSubmit(this.props.item, this.props.content)}>
              <Field
                component='textarea'
                name={this.props.item.title}
                type='textarea'
                value={this.props.content}
                onChange={this.props.changeContent}
                onKeyUp={this.keyHandler}
              />
              <div>
                <Button color='primary' disabled={this.props.pristine || this.props.submitting}>
                  Salvar
                </Button>
                <Button color='primary' onClick={this.props.reset}>
                  Limpar
                </Button>
              </div>
            </Form>
          </PopoverBody>
        </Popover>
        :
        <Popover
          placement="bottom"
          isOpen={this.props.statePover}
          target={`Popover${this.props.item._id}`}
        >
          <PopoverHeader>{this.props.item.title}</PopoverHeader>
          <PopoverBody>
            <FormGroup>
              <Input type='textarea' readOnly value={this.props.item.content}/>
            </FormGroup>
          </PopoverBody>
        </Popover>
      );
    } else return <div />;
  }
}

const mapStateToProps = state => ({
  statePover: state.main.statePover,
  list: state.main.list,
  idPover: state.main.idPover,
  content: state.main.content
});
const mapDispatchToProps = dispatch =>
  bindActionCreators ({openPover, changeContent, handleSubmit}, dispatch);

const formPopover = reduxForm({ form: 'content'})(Popovers)
export default connect (mapStateToProps, mapDispatchToProps) (formPopover);
