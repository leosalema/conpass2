/**
 * Aqui inserimos os estados na aplicação,
 * tanto o estado inicial quanto suas alterações
 * 
 * É feito no INITIAL_STATE uma verificação se já existe registro
 * de alguma lista no localStorage(memória local do browser).
 * 
 * Foi criado um array com no data para manipulação da lista
 * junto ao localStorage para evitar bugs com os registros.
 * 
 * DELETE_HOTSPOT
 * 
 * Foi criada uma verificação para de quantidade de registro,
 * caso seja o último registro a ser excluído é feito a exclusão
 * do localStorage para evitar bugs no retorno da aplicação.
 * 
 * SAVE_CONTENT
 * 
 * É feito uma ordenação no array para manter a lista por ordem de id
 * e não pelo registro no state.
 */

import {combineReducers} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';

const INITIAL_STATE = {
  list: JSON.parse (localStorage.getItem ('@hotspot')) || [],
  open: false,
  buttonInclude: false,
  statePover: false,
  idPover: null,
  content: '',
};
let data = new Array ();

function mainReducer (state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'DELETE_HOTSPOT':
      let list = state.list.filter (item => item._id !== action.payload._id);
      localStorage.setItem ('@hotspot', JSON.stringify (list));
      if (state.list.length === 1) localStorage.removeItem ('@hotspot');
      return {...state, list, content: ''};
    case 'CAPTURE_MOUSE':
      return {...state, open: true};
    case 'CREATE_HOTSPOT':
      let item = action.payload;
      data.push (item);
      localStorage.setItem ('@hotspot', JSON.stringify (data));
      return {...state, open: false, list: [...state.list, action.payload]};
    case 'OPEN_POVER':
      return {...state, statePover: !state.statePover, idPover: action.payload};
    case 'CHANGE_CONTENT':
      return {...state, content: action.payload};
    case 'SAVE_CONTENT':
      list = state.list.filter (item => item._id !== action.payload._id);
      list.push (action.payload);
      list.sort ((a, b) => {
        if (a._id < b._id) return -1;
        if (a._id > b._id) return 1;
        return 0;
      });
      localStorage.setItem ('@hotspot', JSON.stringify (list));
      return {...state, open: false, list, content: '', statePover: false};
    default:
      return state;
  }
}

const rootReducer = combineReducers ({
  main: mainReducer,
  form: reduxFormReducer,
});

export default rootReducer;
