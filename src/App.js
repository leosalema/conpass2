/**
 * 
 * App.js é responsável por criar a store
 * nela ficam contidos todos os dados processados pela
 * aplicação.
 *
 * 
 */

import React from 'react';
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import promise from 'redux-promise'
import reducers from './reducers'
import Main from './views/Main'

const store = applyMiddleware(promise)(createStore)(reducers)

export default props => (
  <Provider store={store}>
    <Main />
  </Provider>
)
