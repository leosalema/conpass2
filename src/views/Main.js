/**
 * O modelo para exibir a home page
 * 
 * Recebe o componente Header
 * 
 * Uma verificação é executada no status de leitura
 * do ponteiro do mouse para registros do clique,
 * se a opção for verdadeiro, o registro da ação do local
 * será acionada e então um registro na tabela será feito e 
 * junto a isso um marcação no local.
 * 
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {createHotspot, captureMouse} from '../actions/mainActions';
import Header from '../components/Header';
import Table from '../components/Table';
import ButtonPulse from '../components/ButtonPulse';
import '../styles/main.css';

class Main extends Component {
  render () {
    if (this.props.open)
      return (
        <div className="container body" onClick={this.props.createHotspot}>
          <Header />
          <button className="button" onClick={this.props.captureMouse}>
            Create Hotspot
          </button>
          <Table />
          {this.props.list === []
            ? <div />
            : this.props.list.map (item => (
                <ButtonPulse key={item._id} item={item} />
              ))}
        </div>
      );
    else
      return (
        <div className="container body">
          <Header />
          <button className="button" onClick={this.props.captureMouse}>
            Create Hotspot
          </button>
          <Table />
          {this.props.list === []
            ? <div />
            : this.props.list.map (item => (
                <ButtonPulse key={item._id} item={item} />
              ))}
        </div>
      );
  }
}

const mapStateToProps = state => ({
  open: state.main.open,
  includeButton: state.includeButton,
  list: state.main.list,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators ({createHotspot, captureMouse}, dispatch);

export default connect (mapStateToProps, mapDispatchToProps) (Main);
