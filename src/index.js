/**
 * Principal arquivo
 * ele é responsável por inserir no index.html
 * toda a aplicação.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<App />, document.getElementById('root'));