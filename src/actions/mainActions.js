/**
 * As ações disparadas pela aplicação estão descritas aqui
 * 
 * É criado um id que recebe 0 e caso o localStorage tem algum registro ele 
 * irá sobreescrever o id
 * 
 * previusId foi criado para fazer o controle do id para o próximo registro.
 * 
 */

let id = 0;
if (localStorage.getItem ('@hotspot') !== null) {
  let list = JSON.parse (localStorage.getItem ('@hotspot'));
  id = list[list.length - 1]._id;
}
let previousId = id;

export const deleteHotspot = item => {
  return {
    type: 'DELETE_HOTSPOT',
    payload: item,
  };
};

export function createHotspot (event) {
  const id = previousId + 1;
  const item = {
    _id: id,
    title: `Hotspot #${id}`,
    top: event.clientY,
    left: event.clientX,
    content: ''
  };
  previousId = id;

  return {
    type: 'CREATE_HOTSPOT',
    payload: item,
  };
}

export function captureMouse () {
  return {
    type: 'CAPTURE_MOUSE',
  };
}

export function openPover (id) {
  return {
    type: 'OPEN_POVER',
    payload: id
  };
}

export function changeContent(event) {
  return {
    type: 'CHANGE_CONTENT',
    payload: event.target.value
  }
}

export function handleSubmit(item, content) {
  const data = {
    _id: item._id,
    title: item.title,
    top: item.top,
    left: item.left,
    content
  }
  return {
    type: 'SAVE_CONTENT',
    payload: data
  }
}